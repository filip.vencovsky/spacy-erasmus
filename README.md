# NLP with SpaCy

- For python version 3

## Environment

### Anaconda or Miniconda

Follow instructions on https://docs.anaconda.com/anaconda/install/

After a successful instalation, update your conda packages using:

```bash
conda update -n base -c defaults conda
```

### Jupyter

Notebooks run in [Jupyter](https://jupyter.org/) or Jupyter Lab.

Jupyter is a part of Anaconda. If not install JupyterLab using this command:

```bash
conda install -c conda-forge jupyterlab
```

## Libraries

### Pandas for data handling

We will load and transform our data using dataframes operations with Pandas[Pandas](https://pandas.pydata.org/) library. Pandas is a standard part of Anaconda installation. If you use Miniconda, it is possible to install pandas by:

```bash
conda install pandas
```

### spaCy for NLP

[spaCy](https://spacy.io) is our main NLP library. Please follow installation instructions on https://spacy.io/usage. For conda based installations use:

```bash
conda install -c conda-forge spacy
conda install -c conda-forge spacy-lookups-data
```

### spaCy language models

Because NLP is based mainly on pretrained language models, we need to dowload the models befere using spaCy.

- medium-size English model is a must have `en_core_web_md`
- optionally large English model `en_core_web_lg`

Installation instructions for language models: https://spacy.io/usage/models

```bash
python -m spacy download en_core_web_md
```

### Keras for classifier training

For advanced modelling techniques, we use Keras. See https://anaconda.org/conda-forge/keras.

```bash
conda install -c conda-forge keras
```

Make sure, `scikit-learn` and `tenserflow` are also installed. They should be keras dependencies.

### Seaborn for visualisations

For data visualization, we use [Seaborn](https://seaborn.pydata.org/index.html) library.

```bash
conda install seaborn
```

